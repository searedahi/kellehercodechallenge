﻿using System;

namespace TransportProj.Factories
{
    public class CityFactory
    {
        public static City Create(int length, int width)
        {
            var city = new City(length, width);
            return city;
        }

        public static City CreateRandom()
        {
            var rand = new Random();
            var cityLength = rand.Next(2,100);
            var cityWidth = rand.Next(2,100);

            var city = new City(cityLength, cityWidth);
            return city;
        }
    }
}
