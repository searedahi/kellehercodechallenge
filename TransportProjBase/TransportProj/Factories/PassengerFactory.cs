﻿using System;

namespace TransportProj.Factories
{
    public class PassengerFactory
    {
        private City _city;

        public PassengerFactory(City city)
        {
            _city = city;
        }

        public Passenger Create(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            var pass = new Passenger(startXPos, startYPos, destXPos, destYPos, _city);
            return pass;
        }

        public Passenger CreateRandom()
        {
            var rand = new Random();
            var pass = new Passenger(rand.Next(_city.Length - 1), rand.Next(_city.Width - 1), rand.Next(_city.Length - 1), rand.Next(_city.Width - 1), _city);
            return pass;
        }
    }
}
