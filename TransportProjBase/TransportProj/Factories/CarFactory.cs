﻿using System;
using System.Runtime.Remoting.Channels;

namespace TransportProj.Factories
{
    public class CarFactory
    {
        private City _city;
        private Passenger _passenger;

        public CarFactory(City city, Passenger passenger)
        {
            _city = city;
            _passenger = passenger;
        }

        public CarFactory(City city)
        {
            _city = city;
        }

        public Car CreateSedan(int xPos, int yPos)
        {
            return new Sedan(xPos, yPos, _city, _passenger);
        }

        public Car CreateRaceCar(int xPos, int yPos, int spacesPerMove)
        {
            return new RaceCar(xPos, yPos, _city, _passenger, spacesPerMove);
        }

        public Car CreateRandom()
        {
            var rand = new Random();
            var carType = rand.Next(3);

            var xPos = rand.Next(_city.Length - 1);
            var yPos = rand.Next(_city.Width - 1);

            switch (carType)
            {
                case 2:
                    var maxSpaces = Math.Max(_city.Length, _city.Width);
                    var randSpaces = rand.Next(2, maxSpaces);
                    return CreateRaceCar(xPos, yPos, randSpaces);
                default:
                    return CreateSedan(xPos, yPos);
            }
        }


    }
}
