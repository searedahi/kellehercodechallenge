﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public CarTypeENUM CarType { get; protected set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        /// <summary>
        /// Move towards the (x,y) target coordinates, first over the x-axis then over the y-axis.
        /// </summary>
        /// <param name="xTarget">Target x coordinate</param>
        /// <param name="yTarget">Target y coordinate</param>
        public abstract void MoveTowardsTarget(int xTarget, int yTarget);

        public virtual void MoveUp()
        {
            if (YPos >= City.Width) return;
            YPos++;
            FinalizeMove();
        }

        public virtual void MoveDown()
        {
            if (YPos <= 0) return;
            YPos--;
            FinalizeMove();
        }

        public virtual void MoveRight()
        {
            if (XPos >= City.Length) return;
            XPos++;
            FinalizeMove();
        }

        public virtual void MoveLeft()
        {
            if (XPos <= 0) return;
            XPos--;
            FinalizeMove();
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }



        public virtual void FinalizeMove()
        {
            WritePositionToConsole();
            PingVeyoServer();
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine("Car moved to ({0},{1})", XPos, YPos);
        }

        public virtual async Task<string> PingVeyoServer()
        {
            // You need to add a reference to System.Net.Http to declare client.
            var client = new WebClient();
            var veyo = new Uri("http://www.veyo.com");
            Task<string> getStringTask = client.DownloadStringTaskAsync(veyo);
            var urlContents = await getStringTask;
            return urlContents;
        }
    }
}
