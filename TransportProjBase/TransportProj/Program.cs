﻿using System;
using System.Threading.Tasks;
using TransportProj.Factories;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            var city = CityFactory.CreateRandom();
            var passenger = new PassengerFactory(city).CreateRandom();
            var car = new CarFactory(city).CreateRandom();

            city.AddCarToCity(car);
            city.AddPassengerToCity(passenger);

            WriteTheStartingInfo(city, car, passenger);

            int tickCount = 0;
            while (!passenger.IsAtDestination())
            {
                Tick(car, passenger);
                tickCount++;
            }

            WriteTheStoppingInfo(tickCount);
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            // Does the car have a passenger?
            if (car.Passenger == null)
            {
                // Did the car arrvie at the passenger starting point last iteration?
                if (car.XPos == passenger.StartingXPos && car.YPos == passenger.StartingYPos)
                {
                    passenger.GetInCar(car);
                    //Console.WriteLine("Picking up the passenger at ({0},{1})",car.XPos, car.YPos);
                }
                else
                {
                    // No?  Move the car toward the passenger
                    car.MoveTowardsTarget(passenger.StartingXPos, passenger.StartingYPos);
                    //Console.WriteLine("No passenger. {0}  is currently at ({1},{2})", car.CarType, car.XPos, car.YPos);
                }
            }
            else
            {
                // We have a passenger, move towrds the destination
                car.MoveTowardsTarget(passenger.DestinationXPos, passenger.DestinationYPos);
                //Console.WriteLine("Passenger moving towards destination ({0},{1})", car.XPos, car.YPos);
            }
        }

        private static async void WriteTheStartingInfo(City city, Car car, Passenger passenger)
        {
            Console.WriteLine("The city is {0} long x {1} wide.", city.Length, city.Width);

            Console.WriteLine("The {0} at ({1},{2}) is  picking up a passenger at ({3},{4}) and going to ({5},{6})", 
                car.CarType,
                car.XPos,
                car.YPos, 
                passenger.StartingXPos, 
                passenger.StartingYPos, 
                passenger.DestinationXPos,
                passenger.DestinationYPos);

            if (car.CarType != CarTypeENUM.RaceCar) return;

            var andretti = (RaceCar)car;
            Console.WriteLine("RaceCar can move {0}", andretti.SpacesPerMove);
        }

        private static async void WriteTheStoppingInfo(int ticks)
        {
            Console.WriteLine("------ The passenger arrived at their destination in {0} ticks.", ticks);
            Console.WriteLine("------ Press any key to exit.");
            Console.ReadKey();
        }

    }
}