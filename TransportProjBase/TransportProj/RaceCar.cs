﻿using System;

namespace TransportProj
{
    /// <summary>
    /// RaceCar must travel at least 2 spaces per move.
    /// </summary>
    public class RaceCar : Car
    {
        public int SpacesPerMove { get; private set; }

        public RaceCar(int xPos, int yPos, City city, Passenger passenger, int spacesPerMove) : base(xPos, yPos, city, passenger)
        {
            if (spacesPerMove < 2) { throw new Exception("Racecars must move at least 2 spaces."); }
            SpacesPerMove = spacesPerMove;
            CarType = CarTypeENUM.RaceCar;
        }
        
        public override void MoveTowardsTarget(int xTarget, int yTarget)
        {
            int count = 0;
            for (int i = 0; i < SpacesPerMove; i++)
            {
                var xDiff = XPos - xTarget;
                if (xDiff != 0)
                {
                    if (XPos > xTarget)
                    {
                        MoveLeft();
                    }
                    else
                    {
                        MoveRight();
                    }
                    count++;
                    continue;
                }

                var yDiff = YPos - yTarget;
                if (yDiff != 0)
                {
                    if (YPos > yTarget)
                    {
                        MoveDown();
                    }
                    else
                    {
                        MoveUp();
                    }
                }

                count++;
                
                if (YPos - yTarget == 0)
                {
                    // We arrived at the target.  Exit the loop.
                    break;
                }
            }

            Console.WriteLine("RaceCar moved {0} spaces in 1 tick.", count);

        }


        protected override void WritePositionToConsole()
        {
            Console.WriteLine("RaceCar at ({0},{1})", XPos, YPos);
        }
    }
}
