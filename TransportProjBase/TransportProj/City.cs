﻿
using System.Collections.Concurrent;

namespace TransportProj
{
    public class City
    {
        public int Width { get; private set; }
        public int Length { get; private set; }

        public ConcurrentQueue<Passenger> Passengers { get; set; }
        public ConcurrentQueue<Car> Cars { get; set; }

        public City(int length, int width)
        {
            Length = length;
            Width = width;
            Passengers = new ConcurrentQueue<Passenger>();
            Cars = new ConcurrentQueue<Car>();
        }

        public Car AddCarToCity(Car car)
        {
            Cars.Enqueue(car);
            return car;
        }

        public Passenger AddPassengerToCity(Passenger passenger)
        {
            Passengers.Enqueue(passenger);
            return passenger;
        }

        public bool RemoveCarToCity(Car car)
        {
            Car carOut;
            var deque = Cars.TryDequeue(out carOut);
            return deque;
        }

        public bool RemovePassengerToCity(Passenger passenger)
        {

            Passenger passOut;
            var deque = Passengers.TryDequeue(out passOut);
            return deque;
        }

    }
}
