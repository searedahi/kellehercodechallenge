﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
            CarType = CarTypeENUM.Sedan;
        }

        public override void MoveTowardsTarget(int xTarget, int yTarget)
        {
            var xDiff = XPos - xTarget;
            if (xDiff != 0)
            {
                if (XPos > xTarget)
                {
                    MoveLeft();
                }
                else
                {
                    MoveRight();
                }
                return;
            }

            var yDiff = YPos - yTarget;
            if (yDiff != 0)
            {
                if (YPos > yTarget)
                {
                    MoveDown();
                }
                else
                {
                    MoveUp();
                }
                return;
            }

            Console.WriteLine("That's curious, the sedan didn't move.  Check your algorithm!  ({0},{1})  targeting ({2},{3})", XPos, YPos, xTarget, yTarget);

        }


        protected override void WritePositionToConsole()
        {
            Console.WriteLine("Sedan at ({0},{1})", XPos, YPos);
        }
    }
}
